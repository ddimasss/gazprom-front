# Gazprom front

## Project setup
```
npm i
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Start api
See [customap-api](https://bitbucket.org/customap/customap-api/src/master/README.md).

## Default login
* admin@admin.com | admin (роль admin)
* superadmin@admin.com | superadmin (роль superadmin)