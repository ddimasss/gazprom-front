const webpack = require('webpack');
const path = require('path');

const PRODUCTION = (process.env.NODE_ENV === 'production')

const port = 50005
const apiHost = 'http://188.168.35.10'

module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: `${apiHost}:${port}`,
        changeOrigin: true,
      },
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src/')
      }
    },
    plugins: [
      new webpack.DefinePlugin({
        DEBUG: JSON.stringify(!PRODUCTION)
      })
    ],
  },
  chainWebpack: config => {
    config.module
      .rule("i18n")
      .resourceQuery(/blockType=i18n/)
      .type('javascript/auto')
      .use("i18n")
        .loader("@kazupon/vue-i18n-loader")
        .end();
  },
  transpileDependencies: [
    'vuetify'
  ],
}