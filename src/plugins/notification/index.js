import notification from './component/notification.vue';
import { registerGlobalComponent, vueUse } from '@/modules/utils';

const vueNotification = {
	install(Vue) {
		if (this.installed) return;
		this.installed = true;
		registerGlobalComponent(Vue, notification);
	}
};

export default vueNotification;

vueUse(vueNotification);