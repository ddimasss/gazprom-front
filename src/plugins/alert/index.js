import alert from './component/alert.vue';
import { registerGlobalComponent, vueUse } from '@/modules/utils';

const vueAlert = {
	install(Vue) {
		if (this.installed) return;
		this.installed = true;
		registerGlobalComponent(Vue, alert);
	}
};

export default vueAlert;

vueUse(vueAlert);