import loading from './component/loading.vue';
import { registerGlobalComponent, vueUse } from '@/modules/utils';

const vueLoading = {
	install(Vue) {
		if (this.installed) return;
		this.installed = true;
		registerGlobalComponent(Vue, loading);
	}
};

export default vueLoading;

vueUse(vueLoading);