/*
* Установить acl уровни доступа
*/
import Acl from './acl';

let acl = new Acl();

Acl.install = (Vue, {router, init, fail, save}) => {
	const bus = new Vue();

  acl.init(router, init, fail, save);

  Vue.prototype.$can = (permission) => acl.check(permission);

	Vue.mixin({
		data() {
			return {
				access: acl.clearPermissions(init)
			};
		},
		watch: {
			access(value) {
				acl.permissions = acl.clearPermissions(value);
				bus.$emit('access-changed', acl.permissions);
				this.$forceUpdate();
			}
		},
		mounted() {
			bus.$on('access-changed', (permission) => (this.access = permission));
		}
	});
};

export default Acl;