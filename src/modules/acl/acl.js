import { isArray } from 'lodash';

/*eslint no-useless-constructor: "off"*/
class Acl {
	constructor() {}

	init(router, permissions, fail) {
		this.router = router;
		this.permissions = this.clearPermissions(permissions);
		this.fail = fail;
	}

	check(permission) {
		if (permission === undefined)
			return false;

		let permissions = (permission.indexOf('|') !== -1) ? permission.split('|') : [permission];

		return this.findPermission(permissions) !== undefined;
	}

	findPermission(pem) {
		return pem.find((permission) => {
			let needed = (permission.indexOf('&') !== -1) ? permission.split('&') : permission;
			if (isArray(needed)) {
				return needed.every(need => (this.permissions.indexOf(need) !== -1));
			}
			return this.permissions.indexOf(needed) !== -1;
		});
	}

	clearPermissions(permissions) {
		if (permissions && permissions.indexOf('&') !== -1)
			permissions = permissions.split('&');
		return isArray(permissions) ? permissions : [permissions];
	}

	set router(router) {
		router.beforeEach((to, from, next) => {
			if (to.meta.permission === 'public')
				return next();

			let fail = to.meta.fail || this.fail || from.fullPath;

			if (!this.check(to.meta.permission))
				return next(fail);

			return next();
		});
	}

	static setAcl(Vue, router, Viewer) {
		Vue.use(Acl, {
			router,
			init: Viewer.group || 'puplic',
			fail: Viewer.isLogged ? '/pages/error' : '/pages/login'
		});
	}
}

export default Acl;