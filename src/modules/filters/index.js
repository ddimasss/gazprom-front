import Vue from 'vue'
import { each } from 'lodash'
import moment from 'moment/moment'

const filters = {
  dateFormat (val, format) {
    return val == null ? '' : moment(val).format(format || 'DD.MM.YY')
  }
}

each(filters, (filter, name) => Vue.filter(name, filter))

export default filters
