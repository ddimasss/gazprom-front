export default {
  start: () => {
    const el = document.getElementById('app');
    if (el) el.innerHTML = 'Загрузка...';
  },
  stop: () => {
    const el = document.getElementById('app');
    if (el) el.innerHTML = '';
  }
}