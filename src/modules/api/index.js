import axios from 'axios';

const VER = '/api/v1'

export default {
  BASE: '',
  PROJECTS: VER + '/projects',
  LOGOUT: VER + '/logout',
  REFRESH: VER + '/refresh',
  LOGIN: VER + '/login',
  ML: VER + '/model',
  ML_FILE: {
    get(query = {}, params = {}) {
      return axios.get(VER + '/files/model/' + query.project_id, {
        params
      })
    }
  },
  GRAPH: {
    // Качество модели (Динамика ROC_AUC)
    getRocAuc(params = {}) {
      return axios.get(VER + '/graph/roc_auc', {
        params
      })
    },
    // Точность прогноза модели в рублях
    getPrecision(params = {}) {
      return axios.get(VER + '/graph/forecast_accuracy', {
        params
      })
    },
    // Процент оттока клиентов
    getCustomerChurn(params = {}) {
      return axios.get(VER + '/graph/customer_churn', {
        params
      })
    },
    // Дашборд
    dashboard(params = {}) {
      return axios.get(VER + '/dashboard', {
        params
      })
    }
  }
}