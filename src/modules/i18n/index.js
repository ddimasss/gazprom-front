// import _ from 'lodash';

import Vue from 'vue';
import VueI18n from 'vue-i18n';
import ru from '@/lang/ru.json';
import en from '@/lang/en.json';
import Locale from '@/modules/Locale';

const DEFAULT = Locale.get();

Vue.use(VueI18n);

let i18n = new VueI18n({
	locale: DEFAULT,
	messages: {
    ru,
    en
  },
	silentTranslationWarn: true,
	fallbackLocale: DEFAULT
});

export default i18n;