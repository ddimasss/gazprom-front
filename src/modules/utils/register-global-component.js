/**
 * Глобальная регистрация плагина компонента
 * @param {object} Vue
 * @param {object} Component definition
 * @example
 * // index.js
 * registerGlobalComponent(Vue, component);
 * // main.js (надо, если автоматическая установка не прошла, дубля не будет)
 * Vue.use(component);
 */
export default (Vue, component) => {
	Vue.component(component.name, component);
	let obj = {};
	obj[`$${component.name}`] = {
		get() {
			let el = this;
			while (el) {
				for (let i = 0; i < el.$children.length; i++) {
					const child = el.$children[i];
					/* istanbul ignore else */
					if (child.$options._componentTag === component.name) {
						return child;
					}
				}
				el = el.$parent;
			}
			if (process.env.NODE_ENV !== 'production') {
				console.warn(`${component.name} component must be part of this component scope or any of the parents scope.`);
			}
			return null;
		}
	};
	Object.defineProperties(Vue.prototype, obj);
}
