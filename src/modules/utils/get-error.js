import { get, isString, isError } from 'lodash';

// сюда пихать обработчики
export default (err) => {
  if (get(err, 'response.data.detail')) {
    return get(err, 'response.data.detail');
  }

  if (isString(err) && !isError(err)) {
    return err;
  }

  if (get(err, 'response.data.message')) {
    return get(err, 'response.data.message');
  }

  if (get(err, 'message')) {
    return get(err, 'message');
  }

  return 'Error';
}