import Locale from '@/modules/Locale';
import { LANG } from '@/modules/constants'

export default (data = {}, opts = {}) => {
	const locale = Locale.current() || LANG.RU;
	return data[locale] || data[LANG.EN] || (opts.default || '–');
};