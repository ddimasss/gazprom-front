/**
* @module getFullName
* @description  Возвращает ФИО из объекта персоны.
* @params  {Obj}
* @return {String}
*
* @example
* // Вернуть полное ФИО
* getUserName({first_name: 'Иван', last_name: 'Иванов', middle_name: 'Иванович'})
*
* // Вернуть инициалы и фамилию
* getUserName({first_name: 'Иван', last_name: 'Иванов', middle_name: 'Иванович', {small: true}})
*
* // Вернуть имя и отчество
* getUserName({first_name: 'Иван', last_name: 'Иванов', middle_name: 'Иванович', {medium: true}})
*
**/

var letterBig = (str) => str.charAt(0).toUpperCase() + str.substr(1).toLowerCase()

export default (data = {}, options = {}) => {
	let name = {
		first_name: (data.first_name) ? `${data.first_name} ` : '',
		last_name: (data.last_name) ? `${data.last_name} ` : '',
		middle_name: (data.middle_name) ? `${data.middle_name} ` : ''
	}

	if (options && options.medium) {
		if (name.first_name) name.first_name = name.first_name.charAt(0).toUpperCase() + '. '
		if (name.middle_name) name.middle_name = name.middle_name.charAt(0).toUpperCase() + '. '
		name = name.first_name + name.middle_name + letterBig(name.last_name)
	} else if (options && options.small) {
		name = letterBig(name.first_name) + letterBig(name.middle_name)
	} else if (options && options.min) {
		name = name.first_name.charAt(0).toUpperCase() + name.middle_name.charAt(0).toUpperCase()
	} else {
		name = letterBig(name.last_name) + letterBig(name.first_name) + letterBig(name.middle_name)
	}
	if (!name.trim()) return options.default ? options.default : 'Нет ФИО'
	return ('' + name).trim()
}
