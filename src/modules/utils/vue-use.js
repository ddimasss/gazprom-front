/**
 * Install plugin if window.Vue available
 * @param {object} Plugin definition
 */
export default (VuePlugin) => {
	if (typeof window !== 'undefined' && window.Vue) {
		window.Vue.use(VuePlugin);
	}
}