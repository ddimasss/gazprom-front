import { isEmail, isMobilePhone } from 'validator';

// @todo прокинуть i18n
export default {
  required: v => !!v || 'Обязательное поле',
  email: v => {
    return !!isEmail(v) || 'Например - admin@admin.com'
  },
  phone: v  => {
    const phone = v.replace(/\D+/g, '');
    const value = '+' + phone;
    return !!isMobilePhone(value) || ''
  }
}
