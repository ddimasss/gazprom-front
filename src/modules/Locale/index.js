import Vue from 'vue';
import { LANG } from '@/modules/constants'
import moment from 'moment/moment';

const DEFAULT = LANG.RU;

export default class Locale {
   static get() {
    let val = Vue.localStorage.get('nlanguage');
    if (!val) {
      val = DEFAULT;
      Vue.localStorage.set('nlanguage', DEFAULT);
    }
    moment.locale(val)
    return val;
  }

  static set(val, inst) {
    if (val) {
      Vue.localStorage.set('nlanguage', val);
    }
    if (inst.$i18n) {
      inst.$i18n.locale = val;
      moment.locale(val);
      location.reload(); // @todo пока так, надо разобраться с обновлением всех компонентов, форсапдейт не пашет.
    }
  }

  static current() {
    return Vue.localStorage.get('nlanguage') || DEFAULT;
  }
}