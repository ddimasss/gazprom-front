// Языки
export const LANG = {
  RU: 'ru',
  EN: 'en'
};

// Типы контакта
export const TYPE_CONTACT_STATUS = {
  WORK: 'WORK', // рабочий
  PERSON: 'PERSON', // личный
};

export const TYPE_CONTACT = {
  EMAIL: 'EMAIL', // почта
  PHONE: 'PHONE', // телефон
};

// Пол
export const SEX_TYPE = {
  MALE: 1, // мужской
  FAMALE: 2, // женский
  ANY: 3, // любой другой
};


// Статусы пользователя
export const PERSON_STATUS_TYPE = {
  NOT_ACTIVE: 1, // не активный
  ACTIVE: 2, // активен
  FIRED: 3, // уволен
  OFF: 4 // в отпуске
};


// Группы
export const GROUP = {
  ADMIN: 'admin', // администратор
};

// Статус проекта
export const PROJECT_STATUS_TYPE = {
  NEW: 1, // Новый
  ACTIVE: 2, // Активный
  NOT_ACTIVE: 3, // Не активный
  TESTING: 4 // Тестирование
};

// Типы моделей
export const PROJECT_MODEL_TYPE = {
  REGRESSION: 1, // Регрессия
  BINARY: 2, // Бинарная классификация
  MULTIPLE: 3, // Множественная классификация
};

// Типы моделей
export const IS_DELETED = {
  DELETED: true, // Удален
  NOT_DELETED: false, // Не удален
};

// Статус проекта
export const PROJECT_STATUS_ML = {
  ACTIVE: 1, // В пром. эксп.
  NOT_ACTIVE: 2, // Отключена
  TESTING: 3 // В тестировании
};
