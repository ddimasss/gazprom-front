/**
 * Реализует доступ к vue модели из js файлов
 * @module Wrapper
 * **/
import Vue from 'vue'

let wrapper = function () {
  let outerResolve
  return {
    __: null,

    wrap (config) {
      wrapper.__ = new Vue(config).$mount('#app')
      outerResolve(wrapper.__)
      return this.promise
    },

    promise: new Promise(resolve => {
      outerResolve = resolve
    })
  }
}

export default wrapper()
