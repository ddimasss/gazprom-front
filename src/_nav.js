export default {
  sidebar: [
    {
      icon: 'mdi-chart-arc',
      text: 'routes.Dashboard',
      path: '/dashboard',
      meta: {
        permission: 'admin|superadmin'
      }
    },
    {
      icon: 'mdi-zip-box',
      text: 'routes.project',
      path: '/project',
      meta: {
        permission: 'admin|superadmin'
      }
    }
  ],
  userMenu: [
    {
      title: 'routes.profile',
      path: '/profile'
    },
    {
      title: 'routes.settings',
      path: '/profile/settings'
    },
    {
      title: 'routes.logout',
      path: '/pages/logout'
    },
  ]
}