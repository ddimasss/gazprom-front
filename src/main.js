import Vue from 'vue'
import App from './App'
import router from './router'
import { vuetify } from './modules/vuetify';
import './modules/vue-localstorage';
import './modules/vue-cookie';
import i18n from './modules/i18n';
import moment from 'moment/moment';
import splashScreen from '@/modules/splash-screen'
import { Viewer } from '@/models'
import wrapper from '@/modules/app-wrapper'
import Acl from '@/modules/acl';
import '@/modules/filters'
import '@/modules/alert';
import '@/modules/loading';
import '@/modules/notification';

Vue.prototype.moment = moment;

Vue.config.productionTip = false;

splashScreen.start();

Viewer.login().finally(() => {
  Acl.setAcl(Vue, router, Viewer);

  splashScreen.stop();

  wrapper.wrap({
    router,
    vuetify,
    i18n,
    render: h => h(App)
  })
});

