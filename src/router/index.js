import Vue from 'vue'
import VueRouter from 'vue-router'
import SingleLayout from '@/layouts/single'
import FullLayout from '@/layouts/full'

import Login from '@/views/auth/Login'
import Logout from '@/views/auth/Logout'
import Error from '@/views/Error'

import ProfileSettings from '@/views/profile/ProfileSettings'
import Profile from '@/views/profile'

import Dashboard from '@/views/Dashboard'
import ProjectList from '@/views/Project'
import ProjectView from '@/views/Project/view'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/dashboard',
    component: FullLayout,
    meta: {
      label: 'Главная',
      permission: 'admin|superadmin'
    },
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
          label: 'routes.Dashboard',
          permission: 'admin|superadmin'
        }
      },
      {
        path: '/project',
        name: 'Project',
        component: ProjectList,
        meta: {
          label: 'routes.project',
          permission: 'admin|superadmin'
        }
      },
      {
        path: '/project/:id',
        name: 'ProjectView',
        component: ProjectView,
        meta: {
          label: 'routes.projectView',
          permission: 'admin|superadmin'
        }
      },
      {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
          label: 'routes.profile',
          permission: 'admin|superadmin'
        }
      },
      {
        path: '/profile/settings',
        name: 'ProfileSettings',
        component: ProfileSettings,
        meta: {
          label: 'routes.ProfileSettings',
          permission: 'admin|superadmin'
        }
      },
    ]
  },
  {
    path: '/pages',
    redirect: '/pages/error',
    name: 'Pages',
    component: SingleLayout,
    meta: {
      label: 'routes.Pages',
    },
    children: [
      {
        path: 'login',
        name: 'login',
        component: Login,
        meta: {
          label: 'routes.login',
          permission: 'public'
        }
      },
      {
        path: 'error',
        name: 'error',
        component: Error,
        meta: {
          label: 'routes.error',
          permission: 'public'
        }
      },
      {
        path: 'logout',
        name: 'logout',
        component: Logout,
        meta: {
          label: 'routes.logout',
          permission: 'public'
        }
      }
    ]
  },
];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
