import { CreateModel } from '@makenskiy/axiosmodel';
import api from '@/modules/api';
import filters from '@/modules/filters'
import { PROJECT_STATUS_TYPE } from '@/modules/constants'

const  PARAMS  = {
  generateProperty:  true,
  base:  api.BASE,
  path:  api.PROJECTS,
  properties: {
    caption: '',
    description: '',
    status: 1,
    model_type: 0,
    ver_model: [],
    employee: '',
    employee_contact: '',
    target_value: '',
    target_caption: ''
  }
};

/**
 * Проекты
 *
 * @export
 * @class ProjestModel
 * @extends {CreateModel}
 */
export default class ProjectModel extends CreateModel {
  constructor(data) {
    super(data);
  }

  static get _opts() {
    return PARAMS;
  }

  /**
   * Дата создания
   *
   * @memberof ProjestModel
   */
  get created_at() {
    return filters.dateFormat(this._data.created_at)
  }

  /**
   * Дата обновления
   *
   * @memberof ProjestModel
   */
  get updated_at() {
    return filters.dateFormat(this._data.updated_at)
  }

  /**
   * Версии моделей
   *
   * @memberof ProjestModel
   */
  get ver_model() {
    return this._data.ver_model
  }

  set ver_model(val) {
    this._data.ver_model = val
  }

  /**
   * Статус
   *
   * @memberof ProjestModel
   */
  get status() {
    let color = null;

    switch (this._data.status) {
      case PROJECT_STATUS_TYPE.NOT_ACTIVE:
        color = null;
      break;
      case PROJECT_STATUS_TYPE.ACTIVE:
        color = 'light-green';
        break;
      case PROJECT_STATUS_TYPE.TESTING:
        color = 'amber';
        break;
      case PROJECT_STATUS_TYPE.NEW:
        color = 'cyan';
      break;
      default:
        color = 'primary';
    }

    return {
      type: this._data.status,
      color
    }
  }
}
