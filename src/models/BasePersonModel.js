import { CreateModel } from '@makenskiy/axiosmodel';
import { get, groupBy, sortBy, forIn } from 'lodash';
import { getUserName } from '@/modules/utils';
import filters from '@/modules/filters'
import { PERSON_STATUS_TYPE } from '@/modules/constants';

/**
 * Базовая модель пользователей
 *
 * @export
 * @class BasePersonModel
 * @extends {CreateModel}
 */
export default class BasePersonModel extends CreateModel {
  constructor(data) {
    super(data);
  }

  /**
   * id
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get id() {
    return get(this._data, '_id')
  }

  /**
   * ФИО
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get fio() {
    return getUserName(this._data);
  }

  /**
   * Аватар
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get avatar() {
    return this._data.avatar
  }

  /**
   * Контакты по типу
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get contact() {
    const contact = this._data.contact || [];
    let res = groupBy(contact.filter(el => !el.isDeleted), 'type')
    forIn(res, (value) => sortBy(value, ['sort', 'status']))
    return res
  }

  /**
   * Пол
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get sex() {
    return this._data.sex
  }

  /**
   * Группы/роли
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get group() {
    return get(this._data, 'group', [])
  }

  /**
   * Фамилия
   *
   * @memberof Viewer
   */
  get last_name () {
    return this._data.lastName
  }

  /**
   * Имя
   *
   * @memberof Viewer
   */
  get first_name () {
    return this._data.firstName
  }

  /**
   * Отчество
   *
   * @memberof Viewer
   */
  get middle_name () {
    return this._data.middleName
  }

  /**
   * День рождения
   *
   * @memberof Viewer
   */
  get birth_day () {
    return filters.dateFormat(this._data.birth_day)
  }

  /**
   * Статус
   *
   * @readonly
   * @memberof BasePersonModel
   */
  get status() {
    let res = {
      type: this._data.status || 0,
      color: null
    }

    switch (res.type) {
      case PERSON_STATUS_TYPE.NOT_ACTIVE:
        res.color = null;
      break;
      case PERSON_STATUS_TYPE.ACTIVE:
        res.color = 'green';
        break;
      case PERSON_STATUS_TYPE.FIRED:
        res.color = 'red';
        break;
      case PERSON_STATUS_TYPE.OFF:
        res.color = 'secondary';
      break;
      default:
        res.color = 'primary';
    }

    return res;
  }

}
