import Vue from 'vue';
import BasePersonModel from './BasePersonModel';
import { GROUP } from '@/modules/constants';
import axios from 'axios'
import api from '@/modules/api';

/**
 * Авторизация
 *
 * @class Viewer
 * @extends {BasePersonModel}
 * @example
 * // в инстантсе vue
 * this.$Viewer
 */
class Viewer extends BasePersonModel{
  constructor(data = {}) {
    super(data);
    this.isLogged = false;
  }

  /**
   * Токены
   *
   * @memberof Viewer
   */
  get token () {
    const token = Vue.cookie.get('token')
    if (token) {
      axios.defaults.headers.common['Authorization'] = token
    }
    return token
  }

  set token (token) {
    if (token) {
      axios.defaults.headers.common['Authorization'] = token
    }
    Vue.cookie.set('token', token)
  }

  get refreshToken () {
    return Vue.cookie.get('refreshToken')
  }

  set refreshToken (token) {
    Vue.cookie.set('refreshToken', token)
  }

  /**
   * Текущий юзер из localStorage
   *
   * @memberof Viewer
   */
  get person() {
		return JSON.parse(Vue.localStorage.get('person')) || '';
	}

	set person(data) {
		Vue.localStorage.set('person', JSON.stringify(data));
  }

  /**
   * Константы доступов
   *
   * @readonly
   * @memberof Viewer
   */
  get access () {
    return {
      dev: 'dev', // дев режим
      public: 'public', // Публичный
      groups: {
        admin: GROUP.ADMIN
      }
    }
  }

  /**
   * Авторизация
   *
   * @param {*} credential
   * @returns
   * @memberof Viewer
   */
  login(credential) {
    return new Promise((resolve, reject) => {
      // передан логин
      if (credential) {
        axios({
          method: 'POST',
          url: api.BASE + api.LOGIN,
          data: credential
        }).then(({data}) => {
          this.token = data.data.token;
          this.refreshToken = data.data.refreshToken;
          this.person = data.data.person;
          this._data = this.person;
          this.isLogged = true;
          resolve(data.data);
        }, err => {
          this.isLogged = false;
          reject(err);
        })
      }

      // проверка токена
      if (!credential && this.refreshToken) {
        axios({
          method: 'POST',
          url: api.BASE + api.REFRESH,
          data: {
            refreshToken: this.refreshToken
          }
        }).then(({data}) => {
          this.token = data.data.token;
          this.refreshToken = data.data.refreshToken;

          setTimeout(() => {
            this.person = data.data.person;
            this._data = this.person;
            this.isLogged = true;
            resolve(data.data);
          })
        }, err => {
          this.isLogged = false;
          reject(err);
        });
      }

      // иначе слать лесом
      if (!credential && !this.token) {
        this.isLogged = false;
        this._data = {};
        reject({
          status: 401,
          message: 'Token not found'
        });
      }
    });
  }

  /**
   * Выход
   *
   * @returns
   * @memberof Viewer
   */
  logout() {
    return axios({
      method: 'POST',
      url: api.BASE + api.LOGOUT,
      data: {
        refreshToken: this.refreshToken
      }
    }).then(({data}) => {
      if (data.data.result) {
        this.token = '';
        this.refreshToken = '';
        this.person = '';
        this._data = {};
        this.isLogged = false;
        return true;
      } else {
        return false;
      }
    }, err => {
      return err
    })
  }
}


Vue.prototype.$Viewer = new Viewer();

export default Vue.prototype.$Viewer;