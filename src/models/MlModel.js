import { CreateModel } from '@makenskiy/axiosmodel';
import api from '@/modules/api';
import filters from '@/modules/filters'
import { PROJECT_STATUS_ML } from '@/modules/constants'
import wrapper from '@/modules/app-wrapper'

const  PARAMS  = {
  generateProperty:  true,
  base:  api.BASE,
  path:  api.ML,
  properties: {
    caption: '',
    status: PROJECT_STATUS_ML.TESTING,
    version: ''
  }
};

let vm = null;
wrapper.promise.then(res => {
  vm = res
})
/**
 * ML
 *
 * @export
 * @class MlModel
 * @extends {CreateModel}
 */
export default class MlModel extends CreateModel {
  constructor(data) {
    super(data);
  }

  static get _opts() {
    return PARAMS;
  }

  /**
   * Дата создания
   *
   * @memberof ProjestModel
   */
  get created_at() {
    return filters.dateFormat(this._data.created_at)
  }

  /**
   * Дата обновления
   *
   * @memberof ProjestModel
   */
  get updated_at() {
    return filters.dateFormat(this._data.updated_at)
  }

  /**
   * Последнее обновление
   *
   * @memberof ProjestModel
   */
  get last_test() {
    return filters.dateFormat(this._data.last_test)
  }

  /**
   * Статус
   *
   * @memberof ProjestModel
   */
  get status() {
    let color = null;

    switch (this._data.status) {
      case PROJECT_STATUS_ML.NOT_ACTIVE:
        color = null;
      break;
      case PROJECT_STATUS_ML.ACTIVE:
        color = 'light-green';
        break;
      case PROJECT_STATUS_ML.TESTING:
        color = 'amber';
        break;
      default:
        color = 'primary';
    }

    return {
      type: this._data.status,
      text: vm ? vm.$t(`mlStatus.${this._data.status}`) : 'none',
      color
    }
  }
}
