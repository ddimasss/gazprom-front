FROM nginx
COPY nginx-v-host.conf /etc/nginx/conf.d/default.conf
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN apt-get -y update
RUN apt-get -y install curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get -y update
RUN apt-get -y install nodejs
RUN node --max_old_space_size=8192
RUN npm cache clean --force
RUN npm i --no-progress --loglevel=error
RUN npm run build
RUN cp -R dist/* /usr/share/nginx/html/